#!/usr/bin/env node

const fetch = require('node-fetch')
const { ArgumentParser } = require('argparse')
const clipboardy = require('clipboardy')

const STRAWPOLL_API_BASE = 'https://www.strawpoll.me/api/v2'

const FRIENDLY_DUP_CHECK_NAMES = {
  ip: 'normal',
  cookie: 'permissive',
  none: 'disabled'
}

async function main () {
  const parser = new ArgumentParser({
    version: require('./package.json').version,
    description: 'Strawpoll',
    prog: 'strawpoll'
  })

  parser.addArgument(
    'title',
    {
      help: 'Title of poll'
    }
  )

  parser.addArgument(
    [ '--option', '-o' ],
    {
      help: 'Options to add to poll',
      action: 'append',
      required: true
    }
  )

  parser.addArgument(
    '--no-clip',
    {
      help: 'Do not copy resulting URL to clipboard',
      action: 'storeTrue',
      defaultValue: false
    }
  )

  parser.addArgument(
    '--multi',
    {
      help: 'Users can vote on multiple options',
      action: 'storeTrue',
      defaultValue: false
    }
  )

  parser.addArgument(
    [ '--duplication-checking', '-d' ],
    {
      help: 'Users can vote on multiple options',
      choices: [ 'ip', 'cookie', 'none' ],
      defaultValue: 'cookie'
    }
  )

  parser.addArgument(
    '--captcha',
    {
      help: 'Poll requires users to pass a CAPTCHA to vote',
      action: 'storeTrue',
      defaultValue: false
    }
  )

  const args = parser.parseArgs()

  if (args.option.length < 2) {
    console.error('Requires at least two options')
    process.exit(1)
  }

  if (args.option.length > 30) {
    console.error('Requires at most 30 options')
    process.exit(1)
  }

  const data = await (
    await fetch(`${STRAWPOLL_API_BASE}/polls`, {
      method: 'POST',
      body: JSON.stringify({
        title: args.title,
        options: args.option,
        multi: args.multi,
        dupcheck: FRIENDLY_DUP_CHECK_NAMES[args.duplication_checking],
        captcha: args.captcha
      })
    })
  )
    .json()

  const resultUrl = `https://strawpoll.me/${data.id}`

  console.log(resultUrl)

  if (!args.no_clip) {
    try {
      await clipboardy.write(resultUrl)
    } catch (_) {
      console.log('Failed to copy to clipboard')
    }
  }
}

main()
