Strawpoll CLI
=============
CLI tool to create strawpolls

![$TRAW POLL - The place to create instant, real-time polls for free.](Strawpoll-cli-large.png)

## What does this do and why would I install it?
It creates polls on [Strawpoll](https://strawpoll.me) in a more efficient way. Useful when gaming and want to ask a server something or don't have a browser open and want to poll an IRC channel.

## Great, now how do I install it?
`sudo npm i -g strawpoll-cli`

## Okay, now how do I use it?
`strawpoll "Example poll" -o "Option one" -o "Option two"`

# Help
```
usage: strawpoll [-h] [-v] --option OPTION [--no-clip] [--multi]
                 [--duplication-checking {ip,cookie,none}] [--captcha]
                 title

Strawpoll

Positional arguments:
  title                 Title of poll

Optional arguments:
  -h, --help            Show this help message and exit.
  -v, --version         Show program's version number and exit.
  --option OPTION, -o OPTION
                        Options to add to poll
  --no-clip             Do not copy resulting URL to clipboard
  --multi               Users can vote on multiple options
  --duplication-checking {ip,cookie,none}, -d {ip,cookie,none}
                        Users can vote on multiple options
  --captcha             Poll requires users to pass a CAPTCHA to vote
```